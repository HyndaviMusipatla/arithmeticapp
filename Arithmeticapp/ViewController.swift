//
//  ViewController.swift
//  Arithmeticapp
//
//  Created by Musipatla,Hyndavi on 4/15/19.
//  Copyright © 2019 Musipatla,Hyndavi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
     var typePicker: [String] = [String]()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return typePicker.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return typePicker[row]
    }
    

    @IBOutlet weak var viewPicker: UIPickerView!
    
   
    
    @IBOutlet weak var WeightTF: UITextField!
    
    
    
    @IBOutlet weak var ExerciseTF: UITextField!
    
    
    @IBOutlet weak var energyconsumedLBL: UILabel!
    
    
    @IBOutlet weak var timetoLoseLBL: UILabel!
    
    
    @IBAction func CalculateBTN(_ sender: UIButton) {
       let value = typePicker[viewPicker.selectedRow(inComponent: 0)]
        WeightTF.text = WeightTF.text!
        ExerciseTF.text = ExerciseTF.text!
        
        energyconsumedLBL.text! = String(format: "%.1f",ExerciseCoach.energyConsumed(during: value , weight: Double(WeightTF.text!)!, time: Int( ExerciseTF.text!)!))
        timetoLoseLBL.text! = String(format: "%.2f",ExerciseCoach.timeToLose1Pound(during: value, weight: Double(WeightTF.text!)!))
        
        
        
        
        
        
        
        
        
    }
    
    @IBAction func ClearBTN(_ sender: UIButton) {
        
        viewPicker.selectRow(0, inComponent: 0, animated: true)
        WeightTF.text = ""
        ExerciseTF.text = ""
        
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        self.viewPicker.delegate = self
        
        self.viewPicker.dataSource = self
        
        typePicker = ["Bicycling", "Jumping rope", "Running - slow", "Running - fast", "Tennis", "Swimming" ]
        
    

    
    
    
    
    
    }


}

